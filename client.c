#include <getopt.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/unistd.h>
#include <stdlib.h>
#include "ipc.h"

int main(int argc, char **argv) {
  int fd;
  struct sockaddr_un addr;
  int ret;
  char buff[8192];
  struct sockaddr_un from;
  int ok = 1;
  int len;

  int c;
  char *action = NULL;
  char *dest = NULL;
  char *path = NULL;

  if ((fd = socket(PF_UNIX, SOCK_DGRAM, 0)) < 0) {
    perror("socket");
    ok = 0;
  }

  if (ok) {
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, CLIENT_SOCK_FILE);
    unlink(CLIENT_SOCK_FILE);
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
      perror("bind");
      ok = 0;
    }
  }

  if (ok) {
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, SERVER_SOCK_FILE);
    if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
      perror("connect");
      ok = 0;
    }
  }

  if (ok) {
    switch (getopt(argc, argv, "a:d:p")) {
    case 'a':
      action = optarg;
      break;
    case 'd':
      dest = optarg;
      break;
    case 'p':
      path = optarg;
      break;
    default:
      break;
    }

	if (action == NULL) {
		ok = 0;
	} else {
      strcpy(buff, action);
      if (send(fd, buff, strlen(buff) + 1, 0) == -1) {
        perror("send");
        ok = 0;
      }
      printf("sent %s\n", buff);
    }
  }

  if (ok) {
    if ((len = recv(fd, buff, 8192, 0)) < 0) {
      perror("recv");
      ok = 0;
    }
    printf("receive %d %s\n", len, buff);
  }

  if (fd >= 0) {
    close(fd);
  }

  unlink(CLIENT_SOCK_FILE);
  return 0;
}
