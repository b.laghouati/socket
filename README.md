# UNIX SOCKET

This is a simple unix socket with a queue implemented in the server



## Getting Started


### Executing program

* Go to your socket directory and build your files

```
make
```
* Open two other terminals and run the following commands in seperate terminals 

```
./bin/server
```

```
./bin/client -a TEST
```



## Acknowledgments

Inspiration, code snippets, etc.
* [Helpful socket unix example](https://github.com/denehs/unix-domain-socket-example.git)

