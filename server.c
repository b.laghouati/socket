#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/unistd.h>

#include "ipc.h"



void exemple() {printf(" test reussi\n");}

struct QueueNode {
  char c[8192];
  struct QueueNode* next;
};

struct Queue {
  struct QueueNode* head;
  struct QueueNode* tail;
};

void initializeQueue(struct Queue* q) {
  q->head = q->tail = NULL;
}

void enqueue(struct Queue* q, const char* c) {
  struct QueueNode* newNode = (struct QueueNode*)malloc(sizeof(struct QueueNode));
  if (newNode == NULL) {
    perror("malloc");
    exit(EXIT_FAILURE);
  }
    strcpy(newNode->c, c);
  newNode->next = NULL;

  if (q->tail == NULL) {
    q->head = q->tail = newNode;
  } else {
    q->tail->next = newNode;
    q->tail = newNode;
  }
}

void dequeue(struct Queue* q, char* c) {
  if (q->head == NULL) {
    strcpy(c, ""); // Empty string indicates an empty queue
    return;
  }

  struct QueueNode* temp = q->head;
  strcpy(c, temp->c);

  q->head = temp->next;

  if (q->head == NULL) {
    q->tail = NULL;
  }

  free(temp);
}

int main(int argc, char **argv) {
  int fd;
  struct sockaddr_un addr;
  int ret;
  char buff[8192];
  struct sockaddr_un from;
  int ok = 1;
  int len;
  socklen_t fromlen = sizeof(from);
  struct Queue commandeQueue;
  initializeQueue(&commandeQueue);

  if ((fd = socket(PF_UNIX, SOCK_DGRAM, 0)) < 0) {
    perror("socket");
    ok = 0;
  }

  if (ok) {
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, SERVER_SOCK_FILE);
    unlink(SERVER_SOCK_FILE);
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
      perror("bind");
      ok = 0;
    }
  }

  while ((len = recvfrom(fd, buff, 8192, 0, (struct sockaddr *)&from,
                         &fromlen)) > 0) {

    if (strcmp(buff, "TEST") == 0) {
      exemple();
      strcpy(buff, " test reussi");
      enqueue(&commandeQueue, buff);
      if (sendto(fd, buff, strlen(buff) + 1, 0, (struct sockaddr *)&from,
                 fromlen) < 0) {
        perror("sendto");
        break;
      }
    } else {
      printf("I don't know this command\n");
      strcpy(buff, "Unknown action");
     
      }

    char commandeResponse[8192];
    dequeue(&commandeQueue, commandeResponse);

      if (sendto(fd, buff, strlen(buff) + 1, 0, (struct sockaddr *)&from,
                 fromlen) < 0) {
      perror("sendto");
  break;
    }
  
  }
  
  while (commandeQueue.head != NULL) {
    struct QueueNode* temp = commandeQueue.head;
    commandeQueue.head = temp->next;
    free(temp);
  }

  if (fd >= 0) {
    close(fd);
  }

  return 0;
}
